const mongoose = require('mongoose')
const User = require('../models/User')
const { ROLE } = require('../constant.js')
mongoose.connect('mongodb://localhost:27017/example')
console.log(mongoose.connection.readyState)
async function clearUser () {
  await User.deleteMany({})
}
const main = async function () {
  await clearUser()
  const user = new User({ username: 'user', password: 'password', role: [ROLE.USER] })
  user.save()
  const admin = new User({ username: 'admin', password: 'password', role: [ROLE.ADMIN, ROLE.USER] })
  admin.save()
}

main().then(function () {
  console.log('finish!')
})
